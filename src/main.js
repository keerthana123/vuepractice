import { createApp } from 'vue'

import App from './App.vue'

import HomeComponent from './components/HomeComponent.vue'

import AboutComponent from './components/AboutComponent.vue'

import PostComponent from './components/PostComponent.vue'

import { createRouter, createWebHistory } from 'vue-router'


const routes = [
    {
      path: '/',
      component: HomeComponent,
    },
    {
      path: '/about',
      component: AboutComponent, 
    },
    {
      path: '/post',
      component: PostComponent,
    },
  ];
  
  const router = createRouter({
    history: createWebHistory(),
    routes: routes,
    linkActiveClass:'active',
  });
  

const app=createApp(App);
app.use(router);
app.mount('#app');